# README #

### What is this repository for? ###

Dit is het library worked example, maar geport naar Android

In dit worked example is één use case uit de bibliotheekcasus, die ook in de lessen software engineering is behandeld, uitgewerkt. Het worked example voldoet aan de referentie-architectuur. Het worked example bevat:

* broncode, uitgewerkt volgens de referentie-architectuur
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This is an Android Studio project.
* The underlying database is a local SQLite database. Android does not support accessing exernal databases directly, as is done in the original library worked example. In Android, this should be done via web services.
* The first time the app is run, the database will be automatically created and provisioned with data.
* If you want to browse the SQLite database, outside the app, read: http://stackoverflow.com/questions/17529766/see-database-filecontent-via-android-studio
* If you want to start all over with a newly created and provisioned database, the easiest way, without needing Android Studio, is to
	* explicitly exit the app
	* remove the database files (library.db and library.db-journal).
        * When you use the emulator, the database is located in /data/data/edu.avans.library/databases. You    can   
          open a commandshell on the emulator using:
          C:\Users\Erco\AppData\Local\Android\sdk\platform-tools>adb devices (gives name of emulator)
          C:\Users\Erco\AppData\Local\Android\sdk\platform-tools>adb -s emulator-xxxx shell (opens the commandshell)
        * When you use a smartphone, you don't have access to /data/data unless you have root access, so then the easiest way is to remove the app from your phone and reinstall it. 
	* restart the app
	* when you then click "find member", onCreate() is called, which will recreate the database and reprovision it with data.
* Another way, using Android Studio, is to
    * increase the database version number "DATABASE_VERSION" in the file LibraryDatabaseHelper
	* explicitly exit the app
	* recompile and restart the app
	* when you then click "find member", onUpgrade() is called which drops the database and then calls onCreate(), which will recreate the database and reprovision it with data.

### Who do I talk to? ###

TODO's

* Unit tests of the original library worked example has not yet been ported to Android
* The "smart" solution using util.MyApplication, to prevent passing the activity context from Activity.onCreate() all the way down to the LibraryDatabaseHelper seem to give exceptions during initialization. Is the database attempted to be opened before Application.onCreate() is called?

### Who do I talk to? ###

* Repo owner