package edu.avans.library.datastorage;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import edu.avans.library.util.MyApplication;

/**
 * @author Erco
 */
public class LibraryDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "library.db";
    private static final int DATABASE_VERSION = 10;

    private static final String CREATE_BOOK_TABLE =
            "CREATE TABLE IF NOT EXISTS `book` (" +
                    "  `ISBN` int(11) NOT NULL," +
                    "  `Title` varchar(64) NOT NULL," +
                    "  `Author` varchar(32) NOT NULL," +
                    "  `Edition` int(11) NOT NULL," +
                    "  PRIMARY KEY (`ISBN`)" +
                    ")";

    private static final String INSERT_BOOK_TABLE =
            "INSERT INTO `book` (`ISBN`, `Title`, `Author`, `Edition`) VALUES" +
                    "(1111, 'Het leven is vurrukkulluk', 'Remco Campert', 1)," +
                    "(2222, 'De Ontdekking van de Hemel', 'Harry Mulisch', 5);";

    private static final String CREATE_COPY_TABLE =
            "CREATE TABLE IF NOT EXISTS `copy` (" +
                    "  `CopyID` int(11) NOT NULL," +
                    "  `LendingPeriod` int(11) NOT NULL," +
                    "  `BookISBN` int(11) NOT NULL," +
                    "  PRIMARY KEY (`CopyID`)," +
                    "  FOREIGN KEY (`BookISBN`) REFERENCES `book` (`ISBN`) ON UPDATE CASCADE" +
                    ")";

    private static final String INSERT_COPY_TABLE =
            "INSERT INTO `copy` (`CopyID`, `LendingPeriod`, `BookISBN`) VALUES" +
                    "(10001, 5, 2222)," +
                    "(10002, 21, 1111);";

    private static final String CREATE_LOAN_TABLE =
            "CREATE TABLE IF NOT EXISTS `loan` (" +
                    "  `ReturnDate` bigint(20) NOT NULL," +
                    "  `MembershipNumber` int(11) NOT NULL," +
                    "  `CopyID` int(11) NOT NULL," +
                    "  PRIMARY KEY (`ReturnDate`,`MembershipNumber`,`CopyID`)," +
                    "  FOREIGN KEY (`MembershipNumber`) REFERENCES `member` (`MembershipNumber`) ON UPDATE CASCADE," +
                    "  FOREIGN KEY (`CopyID`) REFERENCES `copy` (`CopyID`) ON UPDATE CASCADE" +
                    ")";

    private static final String INSERT_LOAN_TABLE =
            "INSERT INTO `loan` (`ReturnDate`, `MembershipNumber`, `CopyID`) VALUES" +
                    "(1445097767507, 1000, 10001);";

    private static final String CREATE_MEMBER_TABLE =
            "CREATE TABLE IF NOT EXISTS `member` (" +
                    " `MembershipNumber` int(11) NOT NULL," +
                    " `FirstName` varchar(32) NOT NULL," +
                    " `LastName` varchar(32) NOT NULL," +
                    " `Street` varchar(32) NOT NULL," +
                    " `HouseNumber` varchar(4) NOT NULL," +
                    " `City` varchar(32) NOT NULL," +
                    " `PhoneNumber` varchar(16) NOT NULL," +
                    " `EmailAddress` varchar(32) NOT NULL," +
                    " `Fine` double NOT NULL," +
                    " `Ssn` bigint(20) NOT NULL," +
                    " PRIMARY KEY (`MembershipNumber`)" +
                    ")";


    private static final String INSERT_MEMBER_TABLE =
            "INSERT INTO `member` (`MembershipNumber`, `FirstName`, `LastName`, `Street`, `HouseNumber`, `City`, `PhoneNumber`, `EmailAddress`, `Fine`, `Ssn`) VALUES" +
                    "(1000, 'Pascal', 'van Gastel', 'Lovensdijkstraat', '61', 'Breda', '0765238754', 'ppth.vangastel@avans.nl', 0, 111222333)," +
                    "(1001, 'Erco', 'Argante', 'Hogeschoollaan', '1', 'Breda', '0765231234', 'e.argante@avans.nl', 0, 111222333)," +
                    "(1002, 'Marice', 'Bastiaensen', 'Lovensdijkstraat', '63', 'Breda', '0765236789', 'mmcm.bastiaensen@avans.nl', 5, 123456782);";

    private static final String CREATE_RESERVATION_TABLE =
            "CREATE TABLE IF NOT EXISTS `reservation` (" +
                    "  `ReservationDate` bigint(20) NOT NULL," +
                    "  `MembershipNumber` int(11) NOT NULL," +
                    "  `BookISBN` int(11) NOT NULL," +
                    "  PRIMARY KEY (`ReservationDate`,`MembershipNumber`,`BookISBN`)," +
                    "  FOREIGN KEY (`MembershipNumber`) REFERENCES `member` (`MembershipNumber`) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "  FOREIGN KEY (`BookISBN`) REFERENCES `book` (`ISBN`) ON UPDATE CASCADE" +
                    ")";

    private static final String INSERT_RESERVATION_TABLE =
            "INSERT INTO `reservation` (`ReservationDate`, `MembershipNumber`, `BookISBN`) VALUES" +
                    "(1445097767507, 1001, 1111);";


    /**
     * Create a helper object to create, open, and/or manage a database. This method always
     * returns very quickly. The database is not actually created or opened until one of
     * getWritableDatabase() or getReadableDatabase() is called.
     *
     */
    public LibraryDatabaseHelper() {
        super(MyApplication.getAppContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }


    public SQLiteDatabase openConnection() {
        try {
            return getWritableDatabase();
        } catch (SQLiteException e) {
            return null;
        }
    }

    /**
     * onCreate() creates and provisions the database.
     * <p/>
     * onCreate() and onUpgrade() callbacks are invoked when the database is actually opened,
     * for example by a call to getWritableDatabase(). The database is not opened when the
     * database helper object itself is created.
     * <p/>
     * onCreate() is only run when the database file did not exist and was just created. If
     * onCreate() returns successfully (doesn't throw an exception), the database is assumed to
     * be created with the requested version number. As an implication, you should not catch
     * SQLExceptions in onCreate() yourself.
     * <p/>
     * See http://stackoverflow.com/questions/21881992/when-is-sqliteopenhelper-oncreate-onupgrade-run
     * for more information.
     *
     * @param db database that will be created
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(this.getClass().getName(), "onCreate() called");
        db.execSQL(CREATE_BOOK_TABLE);
        db.execSQL(INSERT_BOOK_TABLE);
        db.execSQL(CREATE_COPY_TABLE);
        db.execSQL(INSERT_COPY_TABLE);
        db.execSQL(CREATE_LOAN_TABLE);
        db.execSQL(INSERT_LOAN_TABLE);
        db.execSQL(CREATE_MEMBER_TABLE);
        db.execSQL(INSERT_MEMBER_TABLE);
        db.execSQL(CREATE_RESERVATION_TABLE);
        db.execSQL(INSERT_RESERVATION_TABLE);
    }

    /**
     * onUpgrade() drops the existing database and recreates and reprovisions the database.
     * <p/>
     * onCreate() and onUpgrade() callbacks are invoked when the database is actually opened,
     * for example by a call to getWritableDatabase(). The database is not opened when the
     * database helper object itself is created.
     * <p/>
     * onUpgrade() is only called when the database file exists but the stored version number is
     * lower than requested in constructor. The onUpgrade() should update the table schema to
     * the requested version.
     * <p/>
     * See http://stackoverflow.com/questions/21881992/when-is-sqliteopenhelper-oncreate-onupgrade-run
     * for more information.
     *
     * @param db database that will be upgraded
     * @param oldVersion version of the existing database
     * @param newVersion version to the new database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(this.getClass().getName(),
                "onUpgrade() called; from version " + oldVersion + " to version " + newVersion +
                "; dropping old database including data; creating fresh new database");
        db.execSQL("DROP TABLE IF EXISTS `member`");
        onCreate(db);
    }

}