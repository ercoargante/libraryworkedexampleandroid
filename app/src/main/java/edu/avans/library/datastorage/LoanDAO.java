/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.datastorage;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import edu.avans.library.domain.Loan;
import edu.avans.library.domain.Member;
import java.util.List;

/**
 *
 * @author ppthgast
 */
public class LoanDAO {

    private LibraryDatabaseHelper databaseCreator;

    public LoanDAO() {
        databaseCreator = new LibraryDatabaseHelper();
    }

    /**
     * Tries to find the loans for the given in the persistent data store, in
     * this case a MySQL database.In this POC, the lend copies of the books are
     * not loaded - it is out of scope for now.
     *
     * @param member identifies the member whose loans are to be loaded from the
     * database
     *
     * @return an ArrayList object containing the Loan objects that were found.
     * In case no loan could be found, still a valid ArrayList object is
     * returned. It does not contain any objects.
     */
    public List<Loan> findLoans(Member member) {
        List<Loan> loans = new ArrayList<>();

        if (member != null) {
            // First open a database connection
            SQLiteDatabase db = databaseCreator.openConnection();

            if (db != null) {
                // If a connection was successfully setup, execute the SELECT statement.
                int membershipNumber = member.getMembershipNumber();
                Cursor cursor = db.rawQuery("SELECT * FROM loan WHERE MembershipNumber = ?",
                        new String[]{String.valueOf(membershipNumber)});

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        Loan loan = cursorToLoan(cursor, member);
                        // Adding loan to list
                        loans.add(loan);
                    } while (cursor.moveToNext());
                }
                // else there are no loans or an error occurred; leave array list empty.

                // We had a database connection opened. Since we're finished,
                // we need to close it.
                db.close();
            }
        }

        return loans;
    }

    private Loan cursorToLoan(Cursor cursor, Member member) {
        // The value for the CopyID in the row is ignored
        // for this POC: no Copy objects are loaded. Having the
        // Loan objects without the Copy objects will do fine
        // to determine whether the owning Member can be removed.

        // SQLite has no type "Date", so Date stored as milliseconds
        return new Loan(new Date(cursor.getLong(0)), member, null);
    }
}
