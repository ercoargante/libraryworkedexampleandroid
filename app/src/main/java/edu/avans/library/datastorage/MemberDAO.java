/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.datastorage;

import edu.avans.library.domain.Member;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 *
 * @author ppthgast
 */
public class MemberDAO {

    private LibraryDatabaseHelper databaseCreator;

    public MemberDAO() {
        databaseCreator = new LibraryDatabaseHelper();
    }

    /**
     * Tries to find the member identified by the given membership number in the
     * persistent data store, in this case a MySQL database. All loans and
     * reservations for the member are loaded as well. In this POC, the reserved
     * books and/or lend copies of the books are not loaded - it is out of scope
     * for now.
     *
     * @param membershipNumber identifies the member to be loaded from the
     * database
     *
     * @return the Member object to be found. In case member could not be found,
     * null is returned.
     */
    public Member findMember(int membershipNumber) {
        Member member = null;

        // First open a database connection
        SQLiteDatabase db = databaseCreator.openConnection();

        if(db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM member WHERE MembershipNumber = ?",
                    new String[]{String.valueOf(membershipNumber)});
            if(cursor.moveToFirst()) {
                member = cursorToMember(cursor);
            }

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            db.close();
        }
        // else an error occurred; leave 'member' to null.
        return member;
    }

    /**
     * Removes the given member from the database.
     *
     * @param membershipNumber identifies the member to be removed.
     *
     * @return true if execution of the SQL-statement was successful, false
     * otherwise.
     */
    public boolean removeMember(int membershipNumber) {
        // First open the database connection.
        SQLiteDatabase db = databaseCreator.openConnection();

        if(db != null) {
            // Execute the delete statement using the membership number to
            // identify the member row.
            int nrRowsAffected = db.delete("member", "MembershipNumber = ?",
                    new String[]{String.valueOf(membershipNumber)});

            // Finished with the connection, so close it.
            db.close();
            return nrRowsAffected == 1;
        }
        // else an error occurred leave 'member' to null.
        return false;

    }

    /**
     * Currently the database is provisioned using raw SQL. This is an alternative
     * to provision the database.
     * @param member member to be inserted in the database
     * @return true success; false error
     */
    private boolean createMember(Member member) {
        ContentValues values = new ContentValues();
        values.put("MembershipNumber", member.getMembershipNumber());
        values.put("FirstName", member.getFirstName());
        values.put("LastName", member.getLastName());
        values.put("Street", member.getStreet());
        values.put("HouseNumber", member.getHouseNumber());
        values.put("City", member.getCity());
        values.put("PhoneNumber", member.getPhoneNumber());
        values.put("EmailAddress", member.getEmailAddress());
        values.put("Fine", member.getFine());

        // First open a database connection
        SQLiteDatabase db = databaseCreator.openConnection();
        if(db != null) {
            long insertId = db.insert("member", null, values);
            db.close();
            return insertId != -1;
        }
        // else an error occurred
        return false;
    }

    private Member cursorToMember(Cursor cursor) {
        Member member = new Member(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
        member.setStreet(cursor.getString(3));
        member.setHouseNumber(cursor.getString(4));
        member.setCity(cursor.getString(5));
        member.setPhoneNumber(cursor.getString(6));
        member.setEmailAddress(cursor.getString(7));
        member.setFine(cursor.getDouble(8));
        return member;
    }

}
