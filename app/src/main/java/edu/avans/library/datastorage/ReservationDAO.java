/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.datastorage;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;

import edu.avans.library.domain.Member;
import edu.avans.library.domain.Reservation;
import java.util.List;

/**
 *
 * @author ppthgast
 */
public class ReservationDAO {

    private LibraryDatabaseHelper databaseCreator;

    public ReservationDAO() {
        databaseCreator = new LibraryDatabaseHelper();
    }

    /**
     * Tries to find the reservations for the given in the persistent data
     * store, in this case a MySQL database.In this POC, the lend copies of the
     * books are not loaded - it is out of scope for now.
     *
     * @param member identifies the member whose reservations are to be loaded
     * from the database
     *
     * @return an ArrayList object containing the Reservation objects that were
     * found. In case no reservation could be found, still a valid ArrayList
     * object is returned. It does not contain any objects.
     */
    public List<Reservation> findReservations(Member member) {
        List<Reservation> reservations = new ArrayList<>();

        if (member != null) {
            // First open a database connection
            SQLiteDatabase db = databaseCreator.openConnection();

            if (db != null) {
                // If a connection was successfully setup, execute the SELECT statement.
                int membershipNumber = member.getMembershipNumber();
                Cursor cursor = db.rawQuery("SELECT * FROM reservation WHERE MembershipNumber = ?",
                        new String[]{String.valueOf(membershipNumber)});

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        Reservation reservation = cursorToReservation(cursor, member);
                        // Adding reservation to list
                        reservations.add(reservation);
                    } while (cursor.moveToNext());
                }
                // else there are no reservations or an error occurred; leave array list empty.

                // We had a database connection opened. Since we're finished,
                // we need to close it.
                db.close();
            }
        }

        return reservations;
    }

    private Reservation cursorToReservation(Cursor cursor, Member member) {
        // The value for the BookISBN in the row is ignored
        // for this POC: no Book objects are loaded. Having the
        // Reservation objects without the Book objects will do fine
        // to determine whether the owning Member can be removed.

        // SQLite has no type "Date", so Date stored as milliseconds
        return new Reservation(new Date(cursor.getLong(0)), member, null);
    }
}
