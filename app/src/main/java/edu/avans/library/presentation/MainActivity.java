package edu.avans.library.presentation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import edu.avans.library.R;
import edu.avans.library.businesslogic.MemberAdminManager;
import edu.avans.library.businesslogic.MemberAdminManagerImpl;
import edu.avans.library.domain.Member;


public class MainActivity extends Activity {

    private TextView textAreaMemberInfo;
    private Button removeMemberButton;

    // The MemberAdminManagerImpl to delegate the real work (use cases!) to.
    private final MemberAdminManager manager = new MemberAdminManagerImpl();

    // A reference to the last member that has been found. At start up and
    // in case a member could not be found for some membership nr, this
    // field has the value null.
    private Member currentMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        removeMemberButton = (Button) findViewById(R.id.buttonRemoveMember);
        textAreaMemberInfo = (TextView) findViewById(R.id.textviewMemberInfo);

    }

    public void doFindMember(View v) {
        EditText editTextMembershipNr = (EditText) findViewById(R.id.editTextMembershipNr);
        int membershipNr = Integer.parseInt(editTextMembershipNr.getText().toString());

        currentMember = manager.findMember(membershipNr);
        String memberInfo = "Lid niet gevonden";
        boolean memberCanBeRemoved = false;

        if (currentMember != null) {
            String boekenGeleend = "nee";
            if (currentMember.hasLoans()) {
                boekenGeleend = "ja";
            }

            String heeftReserveringen = "nee";
            if (currentMember.hasReservations()) {
                heeftReserveringen = "ja";
            }

            memberInfo
                    = currentMember.getFirstName() + " " + currentMember.getLastName()
                    + "\n"
                    + currentMember.getStreet() + " " + currentMember.getHouseNumber()
                    + "\n"
                    + currentMember.getCity()
                    + "\n"
                    + "\n"
                    + "Boete: " + currentMember.getFine()
                    + "\n"
                    + "\n"
                    + "Heeft boeken geleend: " + boekenGeleend
                    + "\n"
                    + "Heeft reserveringen: " + heeftReserveringen;

            memberCanBeRemoved = currentMember.isRemovable();
        }
        // else memberInfo has already proper value. The button that removes a
        // member from the system needs to be disabled. No work needed for that
        // in the else since the value of memberCanBeRemoved is correct.

        removeMemberButton.setEnabled(memberCanBeRemoved);
        textAreaMemberInfo.setText(memberInfo);
    }

    public void doRemoveMember(View v) {
        if (currentMember != null) {
            String message;
            boolean memberRemoved = manager.removeMember(currentMember);

            if (memberRemoved) {
                message = "Lid is succesvol uitgeschreven";
            } else {
                message = "Er is een fout opgetreden. Het lid is niet uitgeschreven";
            }

            textAreaMemberInfo.setText(message);

            // Reset the currentMember field, since the member it reffered
            // to has been removed from the system.
            currentMember = null;
            removeMemberButton.setEnabled(false);
        }
    }
}
