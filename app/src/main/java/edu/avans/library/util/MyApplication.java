package edu.avans.library.util;

import android.app.Application;
import android.content.Context;
import android.util.Log;

/**
 * @author Erco
 *
 * SQLiteOpenHelper needs the activity context. Rather than passing the context all the way down from
 * Activity.onCreate() to the LibraryDatabaseHelper, a "smarter" solution has been chosen.
 * http://stackoverflow.com/questions/2002288/static-way-to-get-context-on-android
 */
public class MyApplication extends Application {
    private static Context context;

    public void onCreate(){
        Log.i(this.getClass().getName(), "onCreate() called");
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}